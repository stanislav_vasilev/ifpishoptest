Solution to the technical test from IFPI

To solve the provided problem I created a Maven project along with tests to the moving parts of the projects.

Time spent: 2h 40min

to test:
> mvn3 test

to package:
> mvn3 package

to run:
>java -jar target/IfpiTestBasket-0.0.1-SNAPSHOT.jar GolfSet FootballKit TennisRacket CricketBat CricketBat

Output will be presented in the console.

TODO: 
More tests can be done to validate the output and test the more basic models.


