import org.junit.Test;
import org.mockito.Mockito;

import models.Discount;
import models.Order;
import models.Product;
import models.Shop;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;

public class TestOrder {

	@Test
	public void testAddProduct() {
		Product p = new Product("item1", 100l);
		Product p1 = new Product("item2", 100l);
		
		Order order = new Order();
		order.addProduct( p );
		assertEquals(1, order.getProducts().size());
		assertEquals(1, order.getProducts().get("item1").getCount().intValue() );
		
		order.addProduct( p );
		assertEquals(1, order.getProducts().size());
		assertEquals(2, order.getProducts().get("item1").getCount().intValue() );
		
		order.addProduct( p1 );
		assertEquals(2, order.getProducts().size());
		
	}
	
	@Test
	public void testOrderCalculations() {
		Product p = new Product("item0", 100l);
		Product p1 = new Product("item1", 100l);
		
		Order order = new Order();
		order.addProduct( p );
		order.addProduct( p1 );
		
		assertEquals(200l, order.getTotal().longValue() );
		
		Product p2 = new Product("item2", 100l);
		Discount discount;
		discount = new Discount();
		discount.setDiscount(10);
		p2.setDiscount(discount);
		order.addProduct( p2 );
		assertEquals(290l, order.getTotal().longValue() );
		
		discount = new Discount();
		discount.setDiscount(50);
		Map<String, Integer> requirements = new HashMap<String, Integer>();
		requirements.put("item1", 2);
		discount.setRequirements( requirements );
		
		Product p3 = new Product("item3", 100l);
		p3.setDiscount( discount );
		order.addProduct( p3 );
		assertEquals(390l, order.getTotal().longValue() );
		
		order.addProduct( p1 );
		assertEquals(440l, order.getTotal().longValue() );
		
	}
	
}
