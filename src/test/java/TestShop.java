
import org.junit.Test;
import org.mockito.Mockito;

import models.Product;
import models.Shop;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

public class TestShop {
	
	@Test
	public void testAddProduct() {
		List<Product> products = new ArrayList<Product>();
		products.add( new Product("TestProduct", 100l) );
		
		Shop shop = new Shop( products );
		assertEquals(true, shop.hasProduct("TestProduct"));
		assertEquals(false, shop.hasProduct("TestProduct1"));
		assertEquals(true, shop.getProduct("TestProduct").getName().equals("TestProduct"));
	}	
}
