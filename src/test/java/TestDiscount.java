import org.junit.Test;
import org.mockito.Mockito;

import models.Discount;
import models.OrderItem;
import models.Product;
import models.Shop;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;

public class TestDiscount {

	@Test
	public void testNoDiscount() {
		Discount discount;
		discount = new Discount();
		
		OrderItem item1 = mock(OrderItem.class);
		when(item1.getCount()).thenReturn(1);
		
		OrderItem item2 = mock(OrderItem.class);
		when(item2.getCount()).thenReturn(3);
		
		Map<String, OrderItem> orderProducts = new HashMap<String, OrderItem>();
		orderProducts.put("item1", item1);
		orderProducts.put("item2", item2);
		
		assertEquals(false, discount.verify(orderProducts));
		
		discount.setDiscount(10);
		assertEquals(true, discount.verify(orderProducts));
	}
	
	@Test
	public void testNoRequirements() {
		Discount discount;
		discount = new Discount();
		discount.setDiscount(10);
		
		OrderItem item1 = mock(OrderItem.class);
		when(item1.getCount()).thenReturn(1);
		
		OrderItem item2 = mock(OrderItem.class);
		when(item2.getCount()).thenReturn(3);
		
		Map<String, OrderItem> orderProducts = new HashMap<String, OrderItem>();
		orderProducts.put("item1", item1);
		orderProducts.put("item2", item2);
		
		assertEquals(true, discount.verify(orderProducts));
	}
	
	@Test
	public void testWithRequirements() {
		Discount discount;
		discount = new Discount();
		discount.setDiscount(10);
		
		OrderItem item1 = mock(OrderItem.class);
		when(item1.getCount()).thenReturn(1);
		
		OrderItem item2 = mock(OrderItem.class);
		when(item2.getCount()).thenReturn(3);
		
		Map<String, OrderItem> orderProducts = new HashMap<String, OrderItem>();
		orderProducts.put("item1", item1);
		orderProducts.put("item2", item2);
		
		Map<String, Integer> requirements = new HashMap<String, Integer>();
		requirements.put("item1", 2);
		discount.setRequirements( requirements );
		
		assertEquals(false, discount.verify(orderProducts));
		
		
		when(item1.getCount()).thenReturn(2);
		orderProducts.put("item1", item1);
		assertEquals(true, discount.verify(orderProducts));
	}
	
}
