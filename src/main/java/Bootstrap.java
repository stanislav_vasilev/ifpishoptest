import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Discount;
import models.Order;
import models.Product;
import models.Shop;

public class Bootstrap {

	public static void main(String[] args) {
		Shop theShop = loadData();
		Order order = new Order();
		
//		args = new String[]{ "GolfSet", "FootballKit", "TennisRacket", "CricketBat", "CricketBat" };
		
		for(String prodName: args)
			if( theShop.hasProduct(prodName) )
				order.addProduct( theShop.getProduct( prodName ) );
			else
				System.out.println("Product "+prodName+" does not exist");

		System.out.println( order.toString() );
	}
	
	private static Shop loadData() {
		List<Product> products = new ArrayList<Product>();
		Product prod;
		Discount discount;
		Map<String, Integer> discountRequirements;
		
		prod = new Product("CricketBat", 6500l);
		products.add( prod );
		
		prod = new Product("TennisRacket", 8000l);
		discount = new Discount();
		discount.setDiscount(50);
		discountRequirements = new HashMap<String, Integer>();
		discountRequirements.put("CricketBat", 2);
		discount.setRequirements( discountRequirements );
		prod.setDiscount( discount );
		products.add( prod );
		
		prod = new Product("GolfSet", 13000l);
		products.add( prod );
		
		prod = new Product("FootballKit", 10000l);
		discount = new Discount();
		discount.setDiscount(10);
		prod.setDiscount(discount);
		products.add( prod );
		
		Shop theShop = new Shop(products);
		
		return theShop;
	}

}
