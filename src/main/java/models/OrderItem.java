package models;

public class OrderItem {
	private Integer count;
	private Product product;
	
	public OrderItem( Product product ) {
		this.product = product;
		count = 1;
	}
	
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public void addProduct() {
		count++;
	}
	
	public String toString() {
		return product.getName() + " " + count;
	}
	
}