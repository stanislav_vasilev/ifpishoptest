package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Order {

	private Map<String, OrderItem> products;
	private Long total;
	private Long subTotal;
	private Long discountValue;
	private List<OrderDiscountLine> discounts;
	
	public Order() {
		super();
		products = new HashMap<String, OrderItem>();
		total = 0l;
		subTotal = 0l;
		discountValue = 0l;
		discounts = new ArrayList<OrderDiscountLine>();
	}

	public String toString() {
		StringBuilder output = new StringBuilder();
		output.append("Subtotal: £"+ this.subTotal.doubleValue()/100+"\n");
		discounts.forEach((line) -> {
			output.append( line.getProductName() + " " + line.getPercentage()+"% Off: £-"+line.getValue().doubleValue()/100 +"\n");
		});
		output.append("Total: £"+ this.total.doubleValue()/100+"\n");
		return output.toString();
	}
	
	public void addProduct(Product product) {
		if( products.containsKey( product.getName() ) )
			products.get(product.getName()).addProduct();
		else
			products.put(product.getName(), new OrderItem( product ) );
		this.calculateValues();
	}
	
	private void calculateValues() {
		total = 0l;
		subTotal = 0l; 
		discountValue = 0l;
		discounts = new ArrayList<OrderDiscountLine>();
		
		for(String productName: this.products.keySet() ) {
			subTotal += this.products.get(productName).getProduct().getPrice() * this.products.get(productName).getCount().longValue();
			if( this.products.get(productName).getProduct().getDiscount() != null && this.products.get(productName).getProduct().getDiscount().verify( this.products ) ) {
				Double discount =  this.products.get(productName).getProduct().getPrice().doubleValue() * ( this.products.get(productName).getProduct().getDiscount().getDiscount().doubleValue() / 100.0 );
				discountValue += this.products.get(productName).getCount().longValue() * discount.longValue();
				discounts.add( new OrderDiscountLine(productName, this.products.get(productName).getProduct().getDiscount().getDiscount(), discount.longValue()) );
			}
		}
		total = subTotal - discountValue;
	}
	
	public Map<String, OrderItem> getProducts() {
		return products;
	}
	
	public Long getTotal() {
		return total;
	}
	
	public Long getSubTotal() {
		return subTotal;
	}
	
	public Long getDiscountValue() {
		return discountValue;
	}
	
	public List<OrderDiscountLine> getDiscounts() {
		return discounts;
	}
	
}
