package models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Shop {

	private Map<String, Product> products;

	public Shop(List<Product> listProducts) {
		super();
		products = new HashMap<String, Product>();
		listProducts.forEach( (prod) ->  {
			products.put( prod.getName() , prod);
		} );
	}

	public Boolean hasProduct(String name) {
		return products.containsKey(name);
	}
	
	public Product getProduct( String name ) {
		if( hasProduct(name) )
			return products.get( name );
		return null;
	}
	
	
	
}
