package models;

import java.util.Map;

public class Discount {

	private Integer discount;
	
	private Map<String, Integer> requirements;

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Map<String, Integer> getRequirements() {
		return requirements;
	}

	public void setRequirements(Map<String, Integer> requirements) {
		this.requirements = requirements;
	}
	
	public Boolean verify( Map<String, OrderItem> orderItems ) {
		if( discount == null || discount <= 0 )
			return false;
		if( requirements == null )
			return true;
		else {
			for(String prodName: requirements.keySet() ) {
				if( !orderItems.containsKey( prodName ) || requirements.get(prodName) > orderItems.get(prodName).getCount() )
					return false;
			}
		}
		return true;
	}
	
}
