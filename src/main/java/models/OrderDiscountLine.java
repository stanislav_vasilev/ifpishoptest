package models;

public class OrderDiscountLine {
	private String productName;
	private Integer percentage;
	private Long value;
	
	public OrderDiscountLine(String productName, Integer percentage, Long value) {
		super();
		this.productName = productName;
		this.percentage = percentage;
		this.value = value;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getPercentage() {
		return percentage;
	}
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	
}